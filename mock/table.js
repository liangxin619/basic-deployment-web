const Mock = require('mockjs')

// const data = Mock.mock({
//   'items|30': [{
//     id: '@id',
//     title: '@sentence(10, 20)',
//     'status|1': ['published', 'draft', 'deleted'],
//     author: 'name',
//     display_time: '@datetime',
//     pageviews: '@integer(300, 5000)'
//   }]
// })
const data = [{
  id: '1',
  title: 'Docker',
  status: '未检查'
},
{
  id: '2',
  title: 'Mysql',
  status: '未检查'
},{
  id: '3',
  title: 'Devops',
  status: '未检查'
},{
  id: '4',
  title: 'Redis',
  status: '未检查'
},{
  id: '5',
  title: 'RabbitMq',
  status: '未检查'
}]
module.exports = [
  {
    url: '/vue-admin-template/table/list',
    type: 'get',
    response: config => {
      const items = data
      return {
        code: 20000,
        data: {
          total: items.length,
          items: items
        }
      }
    }
  }
]
