# basic-deployment-web

#### 介绍
**服务部署-Vue前端**
basic-deployment-web 可以在一个新的环境下安装部署Docker服务及其基本容器，包括（Mysql、redis、Rabbitmq、naocs等）。
主要思路是通过node调用shell脚本或执行shell命令完成整体部署。
部署脚本未提供。
![输入图片说明](src/assets/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220728160430.png)
![输入图片说明](src/assets/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220728160456.png)
![输入图片说明](src/assets/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220728160521.png)