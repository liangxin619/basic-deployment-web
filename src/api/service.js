import request from "@/utils/request";
const baseurl = process.env.NODE_ENV === "development" ? "/api" : "";

// 获取服务列表
export function getDockerList1(params) {
  return request({
    url: baseurl + "/table/list1",
    method: "get",
    params: params,
  });
}

// 获取服务列表
export function getDockerList2(params) {
  return request({
    url: baseurl + "/table/list2",
    method: "get",
    params: params,
  });
}

// 检查基础配置
export function checkBaseServer(params) {
  return request({
    url: baseurl + "/checkBaseServer",
    method: "get",
    params: params,
  });
}

// 检查Docekr
export function checkDocker(params) {
  return request({
    url: baseurl + "/checkDocker",
    method: "get",
    params: params,
  });
}

// 检查容器
export function checkService(params) {
  return request({
    url: baseurl + "/checkService",
    method: "get",
    params: params,
  });
}

// 获取IP
export function getIp(params) {
  return request({
    url: baseurl + "/getIp",
    method: "get",
    params: params,
  });
}

export function baseConfig(params) {
  return request({
    url: baseurl + "/baseConfig",
    method: "get",
    params: params,
  });
}

// 一键部署
export function deployDocker(params) {
  return request({
    url: baseurl + "/deployDocker",
    method: "get",
    params: params,
  });
}

// 重启
export function restartDocker(params) {
  return request({
    url: baseurl + "/restartDocker",
    method: "get",
    params: params,
  });
}

// 查询基础服务的配置信息
export function serviceConfig(params) {
  return request({
    url: baseurl + "/serviceConfig",
    method: "get",
    params: params,
  });
}

// 配置服务
export function deploy_config(params) {
  return request({
    url: baseurl + "/deploy_config",
    method: "get",
    params: params,
  });
}

// nacos服务登录
export function nacos_login(params) {
  return request({
    url: baseurl + "/nacos_login",
    method: "get",
    params: params,
  });
}

// nacos命名空间列表
export function nacos_searchNamespace(params) {
  return request({
    url: baseurl + "/nacos_searchNamespace",
    method: "get",
    params: params,
  });
}

// nacos服务配置列表
export function nacos_searchConfig(params) {
  return request({
    url: baseurl + "/nacos_searchConfig",
    method: "get",
    params: params,
  });
}
